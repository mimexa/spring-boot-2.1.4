package com.accenture.demo.services.services;

import java.util.List;
import java.util.stream.Collectors;

import org.dozer.DozerBeanMapper;
import org.dozer.MappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.accenture.demo.common.dtos.Movie;
import com.accenture.demo.common.exceptions.NotFoundException;
import com.accenture.demo.persist.dbos.MovieDbo;
import com.accenture.demo.persist.repositories.IMovieRepository;

@Service
public class MoviesService implements IMoviesService {

	@Autowired
	private IMovieRepository repository;

	private DozerBeanMapper mapper = new DozerBeanMapper();

	@Override
	public Page<Movie> getAllMovies(Pageable pageNumber) {
		Page<MovieDbo> moviesDbo = repository.getAllMovies(pageNumber);
		return moviesDbo.map(movie -> mapper.map(movie, Movie.class));
	}

	@Override
	public Movie getMovieById(Long id) throws MappingException, NotFoundException {
		return mapper.map(repository.getMovieById(id), Movie.class);
	}

	@Override
	public Movie updateMovie(Movie movie) {
		MovieDbo movieDbo = mapper.map(movie, MovieDbo.class);
		return mapper.map(repository.updateMovie(movieDbo), Movie.class);
	}

	@Override
	public void deteleMovie(Long id) {
		repository.deleteMovie(id);
	}

	@Override
	public Movie createMovie(Movie movie) {
		MovieDbo movieDbo = mapper.map(movie, MovieDbo.class);
		return mapper.map(repository.createMovie(movieDbo), Movie.class);
	}

	@Override
	public List<Movie> getTop5MoviesOrderByName() {
		return repository.getTop5MoviesOrderByName()
				.stream()
				.map(movieDbo -> mapper.map(movieDbo, Movie.class))
				.collect(Collectors.toList());
	}

}
