package com.accenture.demo.persist.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.accenture.demo.common.exceptions.NotFoundException;
import com.accenture.demo.persist.dbos.MovieDbo;

@Service
public class MovieRepositoryService implements IMovieRepository {

	@Autowired
	private IMovieJpaRepository repository;

	@Autowired
	private IMovieRepositoryForKids kidsRepository;

	@Override
	public Page<MovieDbo> getAllMovies(Pageable pageRequest) {
//		return repository.findAll(pageRequest);
		return kidsRepository.findAllMyMovies(pageRequest);
	}

	@Override
	public MovieDbo getMovieById(Long id) throws NotFoundException {
//		Optional<MovieDbo> omovie = repository.findById(id);
//		if (!omovie.isPresent()) {
//		throw new NotFoundException(String.format("No movie found with id [%d]", id));
//	}
//	return omovie.get();
		MovieDbo omovie = kidsRepository.getMovieById(id);
		if (omovie == null) {
			throw new NotFoundException(String.format("No movie found with id [%d]", id));
		}
		return omovie;
	}

	@Override
	public MovieDbo updateMovie(MovieDbo movie) {
		return repository.save(movie);
	}

	@Override
	public void deleteMovie(Long id) {
		repository.deleteById(id);
	}

	@Override
	public MovieDbo createMovie(MovieDbo movie) {
		return repository.save(movie);
	}

	@Override
	public List<MovieDbo> getTop5MoviesOrderByName() {
		return repository.findTop5ByOrderByName();
	}

}
