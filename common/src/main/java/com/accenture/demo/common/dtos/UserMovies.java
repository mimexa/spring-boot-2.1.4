package com.accenture.demo.common.dtos;

import java.util.List;

public class UserMovies extends User {

	private List<Movie> movies;

	public List<Movie> getMovies() {
		return movies;
	}

	public void setMovies(List<Movie> movies) {
		this.movies = movies;
	}

}
