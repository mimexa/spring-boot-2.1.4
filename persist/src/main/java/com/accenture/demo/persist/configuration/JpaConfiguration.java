package com.accenture.demo.persist.configuration;

import javax.sql.DataSource;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories("com.accenture.demo.persist.repositories")
@EntityScan("com.accenture.demo.persist.dbos")
@ComponentScan({ "com.accenture.demo.persist.repositories" })
public class JpaConfiguration {

	@Bean
	public DataSource dataSource() {
		DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
		dataSourceBuilder.driverClassName("oracle.jdbc.OracleDriver");
		dataSourceBuilder.url("jdbc:oracle:thin:@//localhost:1521/orcl");
		dataSourceBuilder.username("hr");
		dataSourceBuilder.password("oracle");
		return dataSourceBuilder.build();
	}
}
