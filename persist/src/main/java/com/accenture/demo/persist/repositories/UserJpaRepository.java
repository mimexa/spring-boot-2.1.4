package com.accenture.demo.persist.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.accenture.demo.persist.dbos.UserDbo;

public interface UserJpaRepository extends PagingAndSortingRepository<UserDbo, Long> {

}
