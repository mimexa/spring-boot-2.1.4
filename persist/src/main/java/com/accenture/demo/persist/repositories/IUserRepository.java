package com.accenture.demo.persist.repositories;

import com.accenture.demo.common.exceptions.NotFoundException;
import com.accenture.demo.persist.dbos.UserDbo;

public interface IUserRepository {

	UserDbo getUser(Long id) throws NotFoundException;

	UserDbo getUserMovies(Long id) throws NotFoundException;

}
