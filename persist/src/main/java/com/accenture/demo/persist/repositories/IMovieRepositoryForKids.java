package com.accenture.demo.persist.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.accenture.demo.persist.dbos.MovieDbo;

public interface IMovieRepositoryForKids {

	Page<MovieDbo> findAllMyMovies(Pageable pageRequest);

	MovieDbo getMovieById(Long id);

}
