package com.accenture.demo.persist.repositories;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import com.accenture.demo.persist.dbos.MovieDbo;
import com.accenture.demo.persist.specifications.MovieSpecifications;

@Repository
public class MovieRepositoryForKids implements IMovieRepositoryForKids {

	@Autowired
	private IMovieJpaRepositoryForKids repository;

	@Autowired
	private EntityManager manager;

	@Override
	public Page<MovieDbo> findAllMyMovies(Pageable pageRequest) {
		return repository.findAll(MovieSpecifications.movieMatchMinimumAge(10), pageRequest);
	}

	@Override
	public MovieDbo getMovieById(Long id) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<MovieDbo> query = builder.createQuery(MovieDbo.class);
		Root<MovieDbo> root = query.from(MovieDbo.class);
		Predicate matchMinimumAge = builder.lessThan(root.get("minimumAge"), 10);
		Predicate matchId = builder.equal(root.get("id"), id);
		query.where(matchMinimumAge, matchId).orderBy(builder.asc(root.get("name")));
		List<MovieDbo> movies = manager.createQuery(query.select(root)).getResultList();
		MovieDbo movie = null;
		if (!CollectionUtils.isEmpty(movies)) {
			movie = movies.get(0);
		}
		return movie;
	}

}
