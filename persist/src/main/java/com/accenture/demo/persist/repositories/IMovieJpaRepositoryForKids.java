package com.accenture.demo.persist.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.accenture.demo.persist.dbos.MovieDbo;

public interface IMovieJpaRepositoryForKids extends JpaRepository<MovieDbo, Long>, JpaSpecificationExecutor<MovieDbo> {

}
