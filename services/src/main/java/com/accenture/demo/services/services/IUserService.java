package com.accenture.demo.services.services;

import com.accenture.demo.common.dtos.User;
import com.accenture.demo.common.dtos.UserMovies;
import com.accenture.demo.common.exceptions.NotFoundException;

public interface IUserService {

	User getUser(Long id) throws NotFoundException;

	UserMovies getUserMovies(Long id) throws NotFoundException;

}
