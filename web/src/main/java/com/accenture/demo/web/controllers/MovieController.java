package com.accenture.demo.web.controllers;

import java.util.List;

import javax.validation.Valid;

import org.dozer.MappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.accenture.demo.common.dtos.Movie;
import com.accenture.demo.common.exceptions.NotFoundException;
import com.accenture.demo.services.services.IMoviesService;

@RestController
@RequestMapping("/movies")
public class MovieController {

	@Autowired
	private IMoviesService service;

	@Secured({ "ROLE_USER", "ROLE_ADMIN" })
	@GetMapping()
	public Page<Movie> getAllMovies(@RequestParam(value = "pageNumber", required = false) Integer pageNumber) {
		Pageable pageRequest = PageRequest.of(pageNumber, 5);
		return service.getAllMovies(pageRequest);
	}

	@Secured({ "ROLE_USER", "ROLE_ADMIN" })
	@GetMapping("/{id}")
	public Movie getMovieById(@PathVariable("id") Long id) throws MappingException, NotFoundException {
		return service.getMovieById(id);
	}

	@Secured({ "ROLE_ADMIN" })
	@PutMapping()
	public Movie updateMovie(@Valid @RequestBody Movie movie) {
		return service.updateMovie(movie);
	}

	@Secured("ROLE_ADMIN")
	@DeleteMapping("/{id}")
	public void deteleMovie(@PathVariable("id") Long id) {
		service.deteleMovie(id);
	}

	@Secured("ROLE_ADMIN")
	@PostMapping()
	public Movie createMovie(@Valid @RequestBody Movie movie) {
		return service.createMovie(movie);
	}

	@Secured("ROLE_ADMIN")
	@GetMapping("/top")
	public List<Movie> getTopMovies() {
		return service.getTop5MoviesOrderByName();
	}

}
