package com.accenture.demo.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

import com.accenture.demo.persist.configuration.JpaConfiguration;
import com.accenture.demo.web.configuration.MethodSecurityConfiguration;
import com.accenture.demo.web.configuration.OAuth2ResourceServerConfig;

@SpringBootApplication
@ComponentScan({ "com.accenture.demo.web.controllers", "com.accenture.demo.services.services",
		"com.accenture.demo.web.configuration" })
@Import(value = { OAuth2ResourceServerConfig.class, MethodSecurityConfiguration.class, JpaConfiguration.class })
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

}
