package com.accenture.demo.services.services;

import java.util.List;

import org.dozer.MappingException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.accenture.demo.common.dtos.Movie;
import com.accenture.demo.common.exceptions.NotFoundException;

public interface IMoviesService {

	Page<Movie> getAllMovies(Pageable pageRequest);

	Movie getMovieById(Long id) throws MappingException, NotFoundException;

	Movie updateMovie(Movie movie);

	void deteleMovie(Long id);

	Movie createMovie(Movie movie);

	List<Movie> getTop5MoviesOrderByName();

}
