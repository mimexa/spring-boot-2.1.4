package com.accenture.demo.web.controllers;

import java.security.Principal;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.accenture.demo.common.dtos.User;
import com.accenture.demo.common.exceptions.NotFoundException;
import com.accenture.demo.services.services.IUserService;

@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	private IUserService service;

	@Secured({ "ROLE_USER" })
	@GetMapping("/me")
	public Principal getMe(Principal principal) {
		return principal;
	}

	@Secured({ "ROLE_USER" })
	@GetMapping("/me/token")
	public Map<String, Object> getExtraInfo(Authentication auth) {
		OAuth2AuthenticationDetails oauthDetails = (OAuth2AuthenticationDetails) auth.getDetails();
		return (Map<String, Object>) oauthDetails.getDecodedDetails();
	}

	@Secured({ "ROLE_ADMIN" })
	@GetMapping("/{id}")
	public User getUser(@PathVariable("id") Long id,
			@RequestParam(value = "fetchMovies", required = false, defaultValue = "false") String fetchMovies)
			throws NotFoundException {
		if (Boolean.valueOf(fetchMovies)) {
			return service.getUserMovies(id);
		} else {
			return service.getUser(id);
		}
	}

}
