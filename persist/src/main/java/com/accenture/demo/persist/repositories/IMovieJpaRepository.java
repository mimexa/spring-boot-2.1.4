package com.accenture.demo.persist.repositories;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.accenture.demo.persist.dbos.MovieDbo;

public interface IMovieJpaRepository extends PagingAndSortingRepository<MovieDbo, Long> {

	List<MovieDbo> findTop5ByOrderByName();
}
