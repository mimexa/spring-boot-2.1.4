package com.accenture.demo.persist.repositories;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accenture.demo.common.exceptions.NotFoundException;
import com.accenture.demo.persist.dbos.UserDbo;

@Service
public class UserRepositoryService implements IUserRepository {

	@Autowired
	private UserJpaRepository repository;

	@Override
	public UserDbo getUser(Long id) throws NotFoundException {
		Optional<UserDbo> user = repository.findById(id);
		if (!user.isPresent()) {
			throw new NotFoundException(String.format("No user found with id [%d]", id));
		}
		return user.get();
	}

	@Override
	public UserDbo getUserMovies(Long id) throws NotFoundException {
		Optional<UserDbo> user = repository.findById(id);
		if (!user.isPresent()) {
			throw new NotFoundException(String.format("No user found with id [%d]", id));
		}
		return user.get();
	}

}
