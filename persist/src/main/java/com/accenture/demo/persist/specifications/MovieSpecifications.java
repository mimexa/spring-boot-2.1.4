package com.accenture.demo.persist.specifications;

import org.springframework.data.jpa.domain.Specification;

import com.accenture.demo.persist.dbos.MovieDbo;

public final class MovieSpecifications {

	private MovieSpecifications() {

	}

	public static Specification<MovieDbo> movieMatchMinimumAge(Integer age) {
		return (root, query, cb) -> {
			return cb.lessThan(root.get("minimumAge"), age);
		};
	}

}
