package com.accenture.demo.web.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.accenture.demo.common.exceptions.NotFoundException;

@ControllerAdvice
public class MovieControllerAdvice {

	/**
	 * The Logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(MovieControllerAdvice.class);

	/**
	 * Not found.
	 */
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	@ExceptionHandler({ NotFoundException.class })
	public void notFound() {
		LOGGER.error("Not found");
	}

	/**
	 * Technical error.
	 */
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	@ExceptionHandler({ Exception.class })
	public void technicalError() {
		LOGGER.error("Technical error");
	}

}
