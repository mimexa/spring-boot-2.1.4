package com.accenture.demo.persist.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.accenture.demo.persist.dbos.MovieDbo;
import com.accenture.demo.common.exceptions.NotFoundException;

public interface IMovieRepository {

	Page<MovieDbo> getAllMovies(Pageable pageRequest);

	MovieDbo getMovieById(Long id) throws NotFoundException;

	MovieDbo updateMovie(MovieDbo movie);

	void deleteMovie(Long id);

	MovieDbo createMovie(MovieDbo movie);

	List<MovieDbo> getTop5MoviesOrderByName();

}
