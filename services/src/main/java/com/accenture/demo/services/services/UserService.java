package com.accenture.demo.services.services;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accenture.demo.common.dtos.User;
import com.accenture.demo.common.dtos.UserMovies;
import com.accenture.demo.common.exceptions.NotFoundException;
import com.accenture.demo.persist.dbos.UserDbo;
import com.accenture.demo.persist.repositories.IUserRepository;

@Service
public class UserService implements IUserService {

	@Autowired
	private IUserRepository repository;

	private DozerBeanMapper mapper = new DozerBeanMapper();

	@Override
	public User getUser(Long id) throws NotFoundException {
		UserDbo userDbo = repository.getUser(id);
		return mapper.map(userDbo, User.class);
	}

	@Override
	public UserMovies getUserMovies(Long id) throws NotFoundException {
		UserDbo userDbo = repository.getUserMovies(id);
		return mapper.map(userDbo, UserMovies.class);
	}

}
